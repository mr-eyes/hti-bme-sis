<?php
session_start();

function checklogin(){
    //echo "----------checkLogin Function------------";
    if($_SESSION['login']!=1){
        echo "Failed to log in you'll be redirected after 5 seconds";
        header( "refresh:5; url=login.php" );
    }
    else{
        $userID=$_SESSION['userID'];
        echo "User ID: ".$userID;
    }
}

function isLogin(){
    //echo "----------isLogin Function------------";
    if(!empty($_SESSION))
    {
        if($_SESSION['login']==1){
            return true;
        }
        else{
            return false;
        }
    }
}

function loginStatus(){
    if(!empty($_SESSION)){
        if(isset($_SESSION['login'])){
            if($_SESSION['login']==1){
                return "logout";
            }
        }
    }
    return "login";
}

if(!isLogin()){
    header("refresh:0; url=login.php");
}