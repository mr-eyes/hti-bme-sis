<?php

session_start();

// edit this
$connection = mysqli_connect("localhost","","","");

// return subjects associative array







function subjectsList(){

global $connection;

$arrayOfSubjects=Array();

$query = "SELECT * FROM subjects ORDER BY subject_name ASC";

$result = mysqli_query($connection,$query);

if (mysqli_num_rows($result) > 0) {

    $arrayOfSubjects=Array();

    while($row = mysqli_fetch_assoc($result)) {

        $subjectName = $row['subject_name'];

        $subjectCode = $row['subject_code'];

        $arrayOfSubjects[$subjectName]= $subjectCode;

        }

    }

    return $arrayOfSubjects;

}





function getSubjectIDFromCode($subjectCode){

    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_code='$subjectCode'";

    $result = mysqli_query($connection,$query);

    $subjectID=0;

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $subjectID = $row['ID'];

        }



    }

    return $subjectID;

}



function getSubjectNoOfDays($subjectCode){



    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_code='$subjectCode'";

    $result = mysqli_query($connection,$query);

    $days=0;

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $days = intval($row['days']);

        }



    }

    return $days;

}

function openRequestRelatedToStudent($requestID){
    global $connection;
    $studentID=$_SESSION['userID'];
    $query = "SELECT * FROM request_open WHERE student_id='$studentID' AND ID=$requestID";
    $result = mysqli_query($connection,$query);
    if(mysqli_num_rows($result) >0){
        return true;
    }else{
        return false;
    }
}


function transferRequestRelatedToStudent($requestID){
    global $connection;
    $studentID=$_SESSION['userID'];
    $query = "SELECT * FROM request_transfer WHERE student_id='$studentID' AND ID=$requestID";
    $result = mysqli_query($connection,$query);
    if(mysqli_num_rows($result) >0){
        return true;
    }else{
        return false;
    }
}



function getSubjectNameFromCode($subjectCode){

    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_code='$subjectCode'";

    $result = mysqli_query($connection,$query);

    $subjectName="";

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $subjectName = $row['subject_name'];

        }



    }

    return $subjectName;

}



function getSubjectCodeFromName($subjectName){

    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_name='$subjectName'";

    $result = mysqli_query($connection,$query);

    $subjectCode="";

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $subjectCode = $row['subject_code'];

        }



    }

    return $subjectCode;

}



function getSubjectNameFromID($subjectID){

    global $connection;

    $query = "SELECT * FROM subjects WHERE ID='$subjectID'";

    $result = mysqli_query($connection,$query);

    $subjectName="";

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $subjectName = $row['subject_name'];

        }



    }

    return $subjectName;

}



function getSubjectIDFromName($subjectName){

    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_name='$subjectName'";

    $result = mysqli_query($connection,$query);

    $subject_id="";

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $subject_id = $row['ID'];

        }



    }

    return $subject_id;

}







// return studentName from ID

function getStudentNameFromAcademicID($academicID){

global $connection;

    $query = "SELECT * FROM students WHERE academic_id='$academicID'";

    $result = mysqli_query($connection,$query);

    return mysqli_fetch_assoc($result)['name'];

}



function getStudentAcademicIDFromStudentID($studentID){

    global $connection;

    $query = "SELECT * FROM students WHERE ID='$studentID'";

    $result = mysqli_query($connection,$query);

    return mysqli_fetch_assoc(mysqli_query($connection,$query))['academic_id'];

}





function getStudentIDFromAcademicID($academicID){

    global $connection;

    $query = "SELECT * FROM students WHERE academic_id='$academicID'";

    $result = mysqli_query($connection,$query);

    return mysqli_fetch_assoc(mysqli_query($connection,$query))['ID'];

}





function formatSubjectDay($days){



    //$replaceArray=Array("1"=>"Saturday","2"=>"Sunday","3"=>"Monday","4"=>"Tuesday","5"=>"Wednesday","6"=>"Thursday","-"=>" & ");
    $replaceArray=Array("1"=>"D1","2"=>"D2","3"=>"D3","4"=>"D4","5"=>"D5","6"=>"D6","-"=>" & ");

    $newDay="";



    for ($i=0;$i<strlen($days);$i++){

    foreach ($replaceArray as $key => $value){

       if($days[$i] == $key ){

           $newDay .= $value;



            }

        }

    }

    return $newDay;



}





function viewOpenRequestsBySubject($subjectCode){

    global $connection;

    $resultArray=Array();

    $subjectID=getSubjectIDFromCode($subjectCode);

    $query = "SELECT * FROM request_open WHERE subject_id=$subjectID";

    $result = mysqli_query($connection,$query);

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $academicID = getStudentAcademicIDFromStudentID($row['student_id']) ;

            $studentName=getStudentNameFromAcademicID($academicID);

            //$requestDate = date('l jS \of F Y h:i:s A',$row['request_date']);

            $requestDate = date('d-M',$row['request_date']);

            $subject_days=formatSubjectDay($row['subject_days']);

            $fromPeriod = $row['from_period'] ;

            $toPeriod = $row['to_period'];

            $comment = $row['comment'];

            $requestID=$row["ID"];


            $rowArray=Array(

                "Request ID"=>$requestID,

                "Academic ID"=>$academicID,

                "Student Name"=>$studentName,

                "Request Date"=>$requestDate,

                "Day(s)"=>$subject_days,

                "From Period"=>$fromPeriod,

                "To Period"=>$toPeriod,

                "Student Comment"=>$comment

                );

            array_push($resultArray,$rowArray);

        }

    }

    return $resultArray;

}



function viewTransferRequestsBySubject($subjectCode){

    global $connection;

    $resultArray=Array();

    $subjectID=getSubjectIDFromCode($subjectCode);

    $query = "SELECT * FROM request_transfer WHERE subject_id=$subjectID";

    $result = mysqli_query($connection,$query);

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            $academicID = getStudentAcademicIDFromStudentID($row['student_id']) ;

            $studentName=getStudentNameFromAcademicID($academicID);

            //$requestDate = date('l jS \of F Y h:i:s A',$row['request_date']);

            $requestDate = date('d-M',$row['date']);

            $subject_days=formatSubjectDay($row['subject_days']) ;

            $fromPeriod = $row['from_period'] ;

            $toPeriod = $row['to_period'];

            $comment = $row['comment'];

            $requestID=$row["ID"];

            $rowArray=Array(

                "Request ID"=>$requestID,

                "Academic ID"=>$academicID,

                "Student Name"=>$studentName,

                "Request Date"=>$requestDate,

                "Day(s)"=>$subject_days,

                "From Period"=>$fromPeriod,

                "To Period"=>$toPeriod,

                "Student Comment"=>$comment

                );

            array_push($resultArray,$rowArray);

        }

    }

    return $resultArray;

}







// return requestTransfer Table



// return listOfStudents

function studentsList(){

    global $connection;

    $arrayOfStudents=Array();

    $query = "SELECT * FROM students ORDER BY academic_id ASC ";

    $result = mysqli_query($connection,$query);

    if (mysqli_num_rows($result) > 0) {

        $arrayOfStudents=Array();

        while($row = mysqli_fetch_assoc($result)) {

            $studentName = $row['name'];

            $academic_id = $row['academic_id'];

            $arrayOfStudents[$academic_id] = $studentName;

        }

    }

    return $arrayOfStudents;



}

// return StudentInfo

function StudentInfo($academic_id){

    global $connection;

    $query = "SELECT * FROM students WHERE academic_id=$academic_id";

    $result = mysqli_query($connection,$query);

    $studentInfo=Array();

    if (mysqli_num_rows($result) > 0) {

        $arrayOfStudents=Array();

        while($row = mysqli_fetch_assoc($result)) {

            $studentName = $row['name'];

            $academic_id = $row['academic_id'];

            $graduated = $row['graduated'];

            $studentInfo=(Array("Student Name"=>$studentName,"Academic ID"=>$academic_id,"graduated"=>$graduated));

        }

    }

    return $studentInfo;



}



// return ListOfSubjects

function ListOfSubjects(){

    global $connection;

    $arrayOfNames=Array();

    $arrayOfCodes=Array();

    $arrayOfDays=Array();

    $total = array();



    $query = "SELECT * FROM subjects ORDER BY subject_name ASC";

    $result = mysqli_query($connection,$query);

    if (mysqli_num_rows($result) > 0) {

        $arrayOfNames=Array();

        while($row = mysqli_fetch_assoc($result)) {

            $subject_name = $row['subject_name'];

            $subject_code = $row['subject_code'];

            $subject_days = $row['days'];

            array_push($arrayOfNames,$subject_name);

            array_push($arrayOfCodes,$subject_code);

            array_push($arrayOfDays,$subject_days);

        }

        return Array($arrayOfCodes,$arrayOfNames,$arrayOfDays);

    }



    return $arrayOfNames;





}



// --------------------------------------------





function checkIdExistence($id){

    global $connection;

    $query = "SELECT * FROM students WHERE academic_id='$id'";

    $result = mysqli_query($connection,$query);

    if (mysqli_num_rows($result) > 0){

        return true;

    }

    else{

        return false;

    }

}



function checkPassword($id,$password){

    global $connection;

    $query = "SELECT * FROM students WHERE academic_id='$id' AND student_pass='$password'";

    $result = mysqli_query($connection,$query);

    if (mysqli_num_rows($result) > 0){

        //echo "User EXIST";

        return true;

    }

    else{

        return false;

    }

}





function checkCredentials($id,$password){

    if(checkIdExistence($id)){

        if(checkPassword($id,$password)){

            return true;

        }

    }else{



        return false;

    }

}







function requestOpen($subjectCode,$fromPeriod,$toPeriod,$subject_days,$comment){
     global $connection;

    //echo "-------------Request Open Function --->>>> Subject Days $subject_days <<<<------";
    $studentID = $_SESSION['userID'];
    $time = time();
    $subjectID=getSubjectIDFromCode($subjectCode);
$query = "INSERT INTO `request_open` (`ID`, `student_id`, `subject_id`, `request_date`, `approved`, `from_period`, `to_period`, `subject_days`, `comment`) VALUES (NULL, '$studentID', '$subjectID', '$time', '0', '$fromPeriod', '$toPeriod', '$subject_days', '$comment')";

    mysqli_query($connection,$query);
redirect("index.php");

}



function requestTransfer($subjectCode,$fromPeriod,$toPeriod,$subject_days,$comment=""){

    global $connection;
    $studentID = $_SESSION['userID'];
    $time = time();
    $subjectID=getSubjectIDFromCode($subjectCode);
    // Aprroved =0 by default.
    $query = "INSERT INTO `request_transfer` (`ID`, `student_id`, `subject_id`, `subject_days`, `to_period`, `date`, `expired`, `approved`, `admin_id`, `requestcode`, `from_period` , `comment`)

  VALUES (NULL, '$studentID', '$subjectID', '$subject_days', '$toPeriod', '$time', '0', '0', NULL, NULL, '$fromPeriod', '$comment')";
    mysqli_query($connection,$query);
}





function howManyTransferRequests($subjectID){
    global $connection;
    $query = "SELECT subject_id FROM request_transfer WHERE subject_id='$subjectID'";
    $result = mysqli_query($connection,$query);


    return mysqli_num_rows($result);



}



function howManyOpenRequests($subjectID){

    global $connection;

    $query = "SELECT subject_id FROM request_open WHERE subject_id='$subjectID'";

    $results = mysqli_query($connection,$query);

    return mysqli_num_rows($results);

}





function getOpenRequests(){

    global $connection;

    $returnArray=Array();

    $query = "SELECT DISTINCT(subject_id) FROM request_open" ;

    $result = mysqli_query($connection,$query);

    if(mysqli_num_rows($result) > 0){

        while ($row = mysqli_fetch_assoc($result)){



            $noOfRequests = howManyOpenRequests($row['subject_id']);

            $subjectName = getSubjectNameFromID($row['subject_id']);

            $returnArray[$subjectName]=$noOfRequests ;



        }

    }

    return $returnArray;





}



function getTransferRequests(){

    global $connection;

    $returnArray=Array();

    $query = "SELECT DISTINCT(subject_id) FROM request_transfer" ;

    $result = mysqli_query($connection,$query);

    if(mysqli_num_rows($result) > 0){



        while ($row = mysqli_fetch_assoc($result)){

            //echo "### ".$row['subject_id']." ###";

            $noOfRequests = howManyTransferRequests($row['subject_id']);



            $subjectName = getSubjectNameFromID($row['subject_id']);

            //echo "</br>No Of Requests: $noOfRequests for subject: $subjectName</br> ";

            $returnArray[$subjectName]=$noOfRequests ;



        }

    }

    return $returnArray;





}





function isTheSubjectCodeCorrect($subjectCode){

    global $connection;

    $query = "SELECT * FROM subjects WHERE subject_code='$subjectCode'";

    $result=mysqli_query($connection, $query);

    if(mysqli_num_rows($result)>0){

        return true;

    }else{

        return false;

    }

}





function redirect($url)

    {

        if (!headers_sent()) {

            #do php redirect if headers not sent

            header('Location: ' . $url);

            exit;

        } else {

            //If headers are sent...

            //do javascript redirect...

            //if javascript disabled, do html redirect.



            echo '<script type="text/javascript">';

            echo 'window.location.href="' . $url . '";';

            echo '</script>';

            echo '<noscript>';

            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';

            echo '</noscript>';

            exit;

        }

    }





function ev($variable){

    foreach($GLOBALS as $key => $value){

        if($variable===$value){

            echo '<p>($'.$key.') => ('.$value.')</p>';

        }

    }

}



function debug($x){

    echo "~~~</br>--DEBUG: ".ev($x)." --~~~</br>";

}







function checkAdminCredentials($userID,$password){

global $connection;

    $query = "SELECT * FROM superusers WHERE userID='$userID' AND password='$password'" ;

    $result = mysqli_query($connection,$query);

    if(mysqli_num_rows($result)){

        return true;

    }else{

        return false;

    }

}



function  getSuperUserIDFromLoginID($loginID){

    global $connection;



    $query = "SELECT * FROM superusers WHERE userID='$loginID'";

    $result = mysqli_query($connection,$query);

    return mysqli_fetch_assoc($result)['ID'];

}





function ifNotLoggedIn($url){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))
    {
        if($_SESSION['login']==1 && $_SESSION["privileges"]=="student"){
            return true;
        }
        else{
            redirect($url);
        }
    }else{

        redirect($url);

    }

}

function ifAdminNotLoggedIn($url){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))
    {
        if($_SESSION['login']==1 && $_SESSION["privileges"]=="admin"){
            return true;
        }
        else{
            redirect($url);
        }
    }else{

        redirect($url);

    }

}





function getUserOpenRequests(){

    global $connection;

    $userID=$_SESSION['userID'];

    $query = "SELECT * FROM request_open WHERE student_id='$userID' AND approved=0";

    $result = mysqli_query($connection, $query);

    $requestIDs = Array();

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            array_push($requestIDs, $row['ID']);

        }

    }else{

        return false;

    }

    return $requestIDs;



}



function getUserTransferRequests(){

    global $connection;

    $userID=$_SESSION['userID'];

    $query = "SELECT * FROM request_transfer WHERE student_id='$userID' AND approved=0";

    $result = mysqli_query($connection, $query);

    $requestIDs = Array();

    if(mysqli_num_rows($result) > 0){

        while($row = mysqli_fetch_assoc($result)){

            array_push($requestIDs, $row['ID']);

        }

    }else{

        return false;

    }

    return $requestIDs;



}



function editOpenRequest($requestID,$fromPeriod,$toPeriod,$subjectDays)

{

    global $connection;

    $userID=$_SESSION['userID'];

    $time = time();



    $query = "UPDATE `request_open` SET `request_date` = '$time', `from_period` = '$fromPeriod', `to_period` = '$toPeriod', `subject_days` = '$subjectDays' WHERE `request_open`.`ID` = $requestID";



    mysqli_query($connection, $query);



}



function editTransferRequest($requestID,$fromPeriod,$toPeriod,$subjectDays)

{

    global $connection;

    $userID=$_SESSION['userID'];

    $time = time();



    $query = "UPDATE `request_transfer` SET `date` = '$time', `from_period` = '$fromPeriod', `to_period` = '$toPeriod', `subject_days` = '$subjectDays' WHERE `request_transfer`.`ID` = $requestID";



    mysqli_query($connection, $query);

}




function ifLoggedIn($url){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))

    {

        if($_SESSION['login']==1 && $_SESSION["privileges"]=="student"){

            redirect($url);

        }

        else{

            return false;

        }

    }

}

function ifAdminLoggedIn($url){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))

    {

        if($_SESSION['login']==1 && $_SESSION["privileges"]=="admin"){

            redirect($url);

        }

        else{

            return false;

        }

    }

}




function ifAdmin($url="admin/index.php"){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))

    {

        if($_SESSION['privileges']=="admin"){

            redirect($url);

        }

        else{

            return false;

        }

    }

}

function ifStudent(){

    //echo "----------isLogin Function------------";

    if(!empty($_SESSION))

    {

        if($_SESSION['privileges']=="student"){

            redirect("../login.php");

        }

        else{

            return false;

        }

    }

}


function checkDuplicatedRequest($subjectCode,$requestType){
global $connection;
    $subjectID = getSubjectIDFromCode($subjectCode);
    $studentID = $_SESSION["userID"];
    $query = "SELECT * FROM request_".$requestType." WHERE subject_id = '$subjectID' AND student_id = '$studentID'";

    $result = mysqli_query($connection,$query);
    if(mysqli_num_rows($result) > 0){
        return true;
    }else{
        return false;
    }
}
