<?php
require_once "functions.php";

$data = $_POST;
$formType ="";

if(!empty($data["formType"])){ $formType = $data["formType"];}

$result = array();

if($formType == "select-subjectCode"){

$action = '';
if(!empty($data["subjectCode"]) && !empty($data["formType"]) ) {
    $result["subjectName"] = getSubjectNameFromCode($data["subjectCode"]);
    $requestType = $data["formType"];

    if ($data["action"] == "open") {
        $action = 'requestOpen';
    } elseif ($data["action"] == "transfer") {
        $action = 'requestTransfer';
    }
    $daysNo = intval(getSubjectNoOfDays($data["subjectCode"]));
    $formList = generateDaysList($daysNo);
       if(checkDuplicatedRequest($data["subjectCode"],$data["action"])){
           $result["mainForm"] = '<br/><div class="alert alert-danger" role="alert">You need to delete the current <a href="'.$data["action"].'Requests.php"><u>request</u></a> in order to make a new one for this subject</div>';
    }else{
           $result["mainForm"] = generateMainForm($formList, $action, getSubjectNameFromCode($data["subjectCode"]),$data["subjectCode"]);

    }
    echo json_encode($result); // mainForm , subjectName
}
}











// _________________________FUNCTIONS_______________________________________
function generateDaysList($daysNo){


    $_1dayList = <<<EOD
                <option value='1'>Saturday</option>
                <option value='2'>Sunday</option>
                <option value='3'>Monday</option>
                <option value='4'>Tuesday</option>
                <option value='5'>Wednesday</option>
                <option value='6'>Thursday</option>
EOD;


    $_2daysList = <<<EOD
        <option value='1-4'>Saturday & Tuesday</option>
        <option value='2-5'>Sunday & Wednesday</option>
        <option value='3-6'>Monday & Thursday</option>
EOD;


    $daysList ="";

    $listPart1 = <<<EOD
    <!-- Select 1 Day -->
    <div class='form-group'>
        <label class='col-md-4 control-label' for='subject_days'>Select Day(s)</label>
        <div class='col-md-4'>
            <select id='subject_days' name='subject_days' class='form-control'>
                <option  disabled value='' selected>Select Day</option>
                <!-- Days Variable -->
EOD;

    $listPart2 = <<<EOD

<!-- Days Variable -->
            </select>
        </div>
    </div>
EOD;


    if($daysNo == 1){
        $daysList = $_1dayList;
    }elseif ($daysNo == 2){
        $daysList = $_2daysList;
    }

    return $listPart1.$daysList.$listPart2;

}

function generateMainForm($daysList,$action,$subjectName,$subjectCode){
//    echo "<h1>Action: $action</h1>";
    $retString= <<<EOD
    <hr>
    <form name="requestForm" id="requestForm" class='form-horizontal' action='postProcessing.php' method='post' onsubmit="return validateRequestForm()">
                    <fieldset>

                        <!-- Form Name -->
                        <legend><strong>Subject</strong> : {$subjectName} </legend>

                        <!-- HIDDEN FIELDS-->
                        <input id='fromPeriod' type='hidden' name='fromPeriod' value=''>
                        <input id='toPeriod' type='hidden' name='toPeriod' value=''>
                        <input id='postType' type='hidden' name='postType' value='$action'>
                        <input type='hidden' name='subjectCode' value='$subjectCode'>
                        <!-- HIDDEN FIELDS -->


                        {$daysList}

                         <!-- Range slide Disabled Viewer: -->
                         <div class="form-group">
                            <label class="col-md-4 control-label" for="viewPeriod">Periods</label>
                            <div class="col-md-4">
                              <input name="viewPeriods" id="viewPeriod" value="" type="text" disabled class="form-control input-md">
                             </div>
                          </div>

                        <!-- Range slider: -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="ex16b">Select Periods</label>
                            <div class="col-md-4">
                                <input id="ex16b" type="text" class="form-control input-md">
                            </div>
                        </div>





                        <!-- Textarea -->
                        <div class='form-group'>
                            <label class='col-md-4 control-label' for='comment'>Comment</label>
                            <div class='col-md-4'>
                                <textarea class='form-control' id='comment' name='comment'></textarea>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class='form-group'>
                            <label class='col-md-4 control-label' for='submit'></label>
                            <div class='col-md-4'>
                                <button id='submit' name='submit' class='btn btn-primary'>Place Request</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
<script>
    $(document).ready(function() {

        $("#ex16b").change(function(){
            var interval = $('#ex16b').val();
            var fromPeriod = interval[0];
            var toPeriod = interval[2];
            $('#fromPeriod').val(fromPeriod);
            $('#toPeriod').val(toPeriod);
            var periods = 'P' + fromPeriod + " to " + 'P' + toPeriod ;
            $('#viewPeriod').val(periods);
        });


    });

</script>
EOD;


    return $retString;



}
