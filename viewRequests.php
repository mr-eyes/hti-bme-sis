<?php // Page Parameters
include("includes/functions.php");
$page_name="openRequests.php";
include("header.php"); ?>

<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <br/>
                    Request Details
<!--                    <small>Subheading</small>-->
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> Open Requests
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->


<?php
$errorMsg = '<div class="alert alert-danger" role="alert">Something Went Wrong</div>';
$allGood = false;
$requestType ='';
$subjectCode = '';
$legend = '';
$subjectName = '';
$subjectsArray=array();
if(!empty($_GET)){
    if( !empty($_GET["subjectCode"])&& !empty($_GET["requestType"])){
    if($_GET["requestType"]=="open" || $_GET["requestType"]=="transfer"){
        if(in_array($_GET["subjectCode"],ListOfSubjects()[0])){
            // Start Main Process
            $allGood = true;
            $requestType =$_GET["requestType"];
            $subjectCode = $_GET["subjectCode"];
            // End Main Process
        }else{
            echo $errorMsg;
        }
    }else{
        echo $errorMsg;
    }
    }else{
        echo $errorMsg;
    }
}else{
    echo $errorMsg;
}

if($allGood){
    $subjectName = getSubjectNameFromCode($subjectCode);

    if($requestType=="open"){
        $subjectsArray = viewOpenRequestsBySubject($subjectCode);
        $legend = $subjectName." Group Opening";
    }
    elseif($requestType=="transfer"){
        $subjectsArray = viewTransferRequestsBySubject($subjectCode);
        $legend = $subjectName." Group Transferring";
    }
}

//print_r($subjectsArray);
echo <<<EOD

<fieldset>

<!-- Form Name -->
<legend>{$legend}</legend>

<table class="table table-sm table-striped">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Periods</th>
      <th>Day(s)</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
EOD;


for($i=0;$i < count($subjectsArray);$i++){
    $academicID = $subjectsArray[$i]["Academic ID"];
    $studentName = ltrim($subjectsArray[$i]["Student Name"]);
    $originalName = $studentName;
    $studentName = explode(" ",$studentName);
    $len = count($studentName);
    array_splice($studentName,3);
    $newName = implode(" ",$studentName);
    $requestID = $subjectsArray[$i]["Request ID"];

//    print_r($studentName);
    $periods = "P".$subjectsArray[$i]["From Period"]." - "."P".$subjectsArray[$i]["To Period"];
    $date = $subjectsArray[$i]["Request Date"];
    $subjectDays = $subjectsArray[$i]["Day(s)"];
    echo <<<EOT
    <tr>
      <td>{$academicID}</td>
      <td>{$newName}</td>
      <td>{$periods}</td>
      <td>{$subjectDays}</td>

EOT;


if($requestType == "open"){





    if(openRequestRelatedToStudent($subjectsArray[$i]["Request ID"]) || $_SESSION['userID']==64){
        echo <<<EOT
                <td>
                    <form method='post' action='deleteRequest.php' >
                    <input type='hidden' name='type' value='{$requestType}'>
                    <input type='hidden' name='requestID' value='{$requestID}'>
                    <input type='hidden' name='redirect' value='viewRequests.php?subjectCode={$subjectCode}&requestType={$requestType}'>
                    <!-- <input type='submit' value='Delete'> -->

                    <button type="submit" value='Delete' class="btn btn-sm btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>


                    </form>
              </td>
EOT;
    }else{
        echo "<td></td>";
    }



}elseif($requestType == "transfer"){
    if(transferRequestRelatedToStudent($subjectsArray[$i]["Request ID"]) || $_SESSION['userID']==64){
        echo <<<EOT
                <td>
                    <form method='post' action='deleteRequest.php' >
                    <input type='hidden' name='type' value='{$requestType}'>
                    <input type='hidden' name='requestID' value='{$requestID}'>
                    <input type='hidden' name='redirect' value='viewRequests.php?subjectCode={$subjectCode}&requestType={$requestType}'>
                    <!-- <input type='submit' value='Delete'> -->

                    <button type="submit" value='Delete' class="btn btn-sm btn-danger">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>

                    </form>
              </td>
EOT;
    }else{
        echo "<td></td>";
    }

}
    echo '</tr>';

}

echo <<<EOD
  </tbody>
</table>
</fieldset>

EOD;


?>





    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
