<?php ifNotLoggedIn("login.php"); ?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HTI | BME</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>



<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Biomedical Engineering Department - HTI</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
<!--                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>-->
                <ul class="dropdown-menu message-dropdown">
<!--                    <li class="message-preview">-->
<!--                        <a href="#">-->
<!--                            <div class="media">-->
<!--                                    <span class="pull-left">-->
<!--                                        <img class="media-object" src="http://placehold.it/50x50" alt="">-->
<!--                                    </span>-->
<!--                                <div class="media-body">-->
<!--                                    <h5 class="media-heading"><strong>John Smith</strong>-->
<!--                                    </h5>-->
<!--                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>-->
<!--                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="message-preview">-->
<!--                        <a href="#">-->
<!--                            <div class="media">-->
<!--                                    <span class="pull-left">-->
<!--                                        <img class="media-object" src="http://placehold.it/50x50" alt="">-->
<!--                                    </span>-->
<!--                                <div class="media-body">-->
<!--                                    <h5 class="media-heading"><strong>John Smith</strong>-->
<!--                                    </h5>-->
<!--                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>-->
<!--                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="message-preview">-->
<!--                        <a href="#">-->
<!--                            <div class="media">-->
<!--                                    <span class="pull-left">-->
<!--                                        <img class="media-object" src="http://placehold.it/50x50" alt="">-->
<!--                                    </span>-->
<!--                                <div class="media-body">-->
<!--                                    <h5 class="media-heading"><strong>John Smith</strong>-->
<!--                                    </h5>-->
<!--                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>-->
<!--                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="message-footer">-->
<!--                        <a href="#">Read All New Messages</a>-->
<!--                    </li>-->
                </ul>
            </li>
<!--            <li class="dropdown">-->
<!--                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>-->
<!--                <ul class="dropdown-menu alert-dropdown">-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>-->
<!--                    </li>-->
<!--                    <li class="divider"></li>-->
<!--                    <li>-->
<!--                        <a href="#">View All</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> &nbsp; <?php echo getStudentAcademicIDFromStudentID($_SESSION['userID']); ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
<!--                    <li>-->
<!--                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>-->
<!--                    </li>-->
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>





        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="<?php if($page_name=='index.php') echo 'active'; ?>">
                    <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>

                <li <?php if($page_name =='requestOpen.php' || $page_name =='requestTransfer.php') echo 'class="active"'; ?>>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> New Request <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="nav navbar-nav <?php if($page_name !='requestOpen.php' && $page_name !='requestTransfer.php') echo 'collapse'; ?>">
                        <li <?php if($page_name=='requestOpen.php') echo 'class="active"'; ?>>
                            <a href="requestOpen.php"><i class="fa fa-fw fa-edit"></i>Request Opening</a>
                        </li>
                        <li <?php if($page_name=='requestTransfer.php') echo 'class="active"'; ?>>
                            <a href="requestTransfer.php"><i class="fa fa-fw fa-edit"></i>Request Transferring</a>
                        </li>

                    </ul>
                </li>


                <li <?php if($page_name =='transferRequests.php' || $page_name =='openRequests.php') echo 'class="active"'; ?>>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i> Subjects Requests <span class="label label-default"><?php echo count(getTransferRequests()) + count(getOpenRequests()) ?></span>
                        <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo2" class="nav navbar-nav <?php if($page_name !='transferRequests.php' && $page_name !='openRequests.php') echo 'collapse'; ?>">
                        <li <?php if($page_name=='openRequests.php') echo 'class="active"'; ?>>
                            <a href="openRequests.php">Open <span class="label label-default"><?php echo count(getOpenRequests()); ?></span></a>
                        </li>
                        <li <?php if($page_name=='transferRequests.php') echo 'class="active"'; ?>>
                            <a href="transferRequests.php">Transfer <span class="label label-default"><?php echo count(getTransferRequests()); ?></span></a>
                        </li>

                    </ul>
                </li>

                <li <?php if($page_name=='tables.php') echo 'class="active"'; ?>>
                    <a href="tables.php"><i class="fa fa-fw fa-table"></i> Tables</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <style>

        #page-wrapper{
            padding-bottom: 90%;

        }


    </style>

