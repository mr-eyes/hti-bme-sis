<?php ifAdminNotLoggedIn("login.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->

</head>



<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Biomedical Engineering Department - HTI</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <ul class="dropdown-menu message-dropdown">
                </ul>
            </li>
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>





        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="<?php if($page_name=='index.php') echo 'active'; ?>">
                    <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>




                <li <?php if($page_name =='transferRequests.php' || $page_name =='openRequests.php') echo 'class="active"'; ?>>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i> Subjects Requests <span class="label label-default"><?php echo count(getTransferRequests()) + count(getOpenRequests()) ?></span>
                        <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo2" class="nav navbar-nav <?php if($page_name !='transferRequests.php' && $page_name !='openRequests.php') echo 'collapse'; ?>">
                        <li <?php if($page_name=='openRequests.php') echo 'class="active"'; ?>>
                            <a href="openRequests.php">Open <span class="label label-default"><?php echo count(getOpenRequests()); ?></span></a>
                        </li>
                        <li <?php if($page_name=='transferRequests.php') echo 'class="active"'; ?>>
                            <a href="transferRequests.php">Transfer <span class="label label-default"><?php echo count(getTransferRequests()); ?></span></a>
                        </li>

                    </ul>
                </li>

                <li <?php if($page_name=='tables.php') echo 'class="active"'; ?>>
                    <a href="tables.php"><i class="fa fa-fw fa-table"></i> Tables</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <style>

        #page-wrapper{
            padding-bottom: 90%;

        }


    </style>