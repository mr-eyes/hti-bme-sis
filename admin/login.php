<?php require_once "../includes/functions.php";
ifAdminLoggedIn("index.php");
?>
<script src="../js/validation.js"></script>

<script>
    $(document).ready(function() {

        $('input').keydown(function(e) {
            if (e.keyCode == 13) {
                $(this).closest('form').submit();
            }
        });
    });
</script>



<?php
$msg ="";

if(!empty($_GET["red"])){
    $msg = "Wrong ID or password";
}


if(!empty($_POST)){

    $userID = $_POST['superUserID'];
    $userPassword = $_POST['superUserPassword'];
    if(checkAdminCredentials($userID,$userPassword)){

        $_SESSION['login']=1;

        $_SESSION['privileges']="admin";

        $_SESSION['userID']=getSuperUserIDFromLoginID($userID);

        redirect("index.php");

    }else{
        echo "<script>alert('Wrong Username Or Password');</script>";
        redirect("login.php?red=retry");
    }

}


else{
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>

        <meta charset="utf-8">
        <title>Admin login</title>
        <meta name="description" content="">
        <meta name="author" content="Padmanaban, Mine Web Design">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/loginStyle.css">

    </head>
    <body>
    <div class="row">
        <div class="container">
            <h2 class="title">BME System - HTI</h2>
            <form class="form-signin" method="post" action="login.php" name="loginForm" onsubmit="return validateLoginForm()">
                <h4 class="section-heading"><span>Admin Login</span></h4>
                <div class="row">
                    <div class="column">
                        <label>ID </label>
                        <input id="academicID" class="full-width" type="text" autocomplete="off" maxlength="9"  name="superUserID" placeholder="ID" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required autofocus >
                    </div>
                </div>
                <div class="row">
                    <div class="column">
                        <label>Password </label>
                        <input id="password" class="full-width" type="password" autocomplete="off" maxlength="8"  name="superUserPassword" placeholder="Password"

                               onkeypress="return (event.charCode >=48 && event.charCode <=57) || ((event.charCode >=65 && event.charCode <=90)) || ((event.charCode >=97 && event.charCode <=122)) || event.charCode ==95" required>
                    </div>
                </div>
                <br/>
                <input class="button-submit" type="submit" value="SIGN IN">
                <!---->
                <div class="row">
                    <!--                <a href="#" class="forgot">Forgot your password?</a>-->
                    <a style="color: red;" class="forgot"><?php echo $msg;?></a>
                </div>
            </form>
        </div>
    </div>
    </body>
    </html>

    <?php
}?>