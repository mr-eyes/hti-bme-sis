<?php // Page Parameters
include("../includes/functions.php");
$page_name="transferRequests.php";
include("header.php");

$transfer = getTransferRequests();



echo "<script>console.log('".json_encode($transfer)."');</script>";


?>

<style>
    tr { cursor: pointer; cursor: hand; }
</style>

<div id="page-wrapper">

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <br/>
                    Transfer requests
                    <small>per subject</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> Transfer Requests
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->








        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Subject Name</th>
                <th>Requests No.</th>
<!--                <th>Username</th>-->
            </tr>
            </thead>
            <tbody>

          <?php
            $count=1;
          foreach ($transfer as $subjectName => $No){
              $subjectCode = getSubjectCodeFromName($subjectName);
              echo <<<EOT
            <tr class="clickable-row" data-href='viewRequests.php?subjectCode={$subjectCode}&requestType=transfer'>
                <th scope="row">{$count}</th>
                <td>{$subjectName}</td>
                <td>{$No}</td>
            </tr>
EOT;
$count++;
          }
          ?>




            </tbody>
        </table>










    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

</body>

</html>


<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.document.location = $(this).data("href");
        });
    });
</script>
