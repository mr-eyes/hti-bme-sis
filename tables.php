<?php // Page Parameters
require_once "includes/functions.php";
$page_name = 'tables.php';
include ("header.php"); ?>

<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/bootstrap-slider.css" rel="stylesheet" type="text/css">

<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap-slider.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>


<style>
    @import url("//fonts.googleapis.com/css?family=Roboto:500");
    @import url("//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css");
    #xlsDownload {
        padding: 30px 0;
        text-align: center;
    }

    .face-button {
        height: 64px;
        display: inline-block;
        border: 3px solid #41cb66;
        font-family: 'Roboto', sans-serif;
        font-size: 20px;
        font-weight: 500;
        text-align: center;
        text-decoration: none;
        color: #41cb66;
        overflow: hidden;

    }
    .face-button .icon {
        margin-right: 6px;
    }
    .face-button .face-primary,
    .face-button .face-secondary {
        display: block;
        padding: 0 32px;
        line-height: 64px;
        transition: margin .4s;
    }
    .face-button .face-primary {
        background-color: #41cb66;
        color: #fff;
    }
    .face-button:hover .face-primary {
        margin-top: -64px;
    }

</style>






<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <br/>
                    Download tables
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> tables
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- -->




        <div id="xlsDownload">

            <a class="face-button" href="files/table.xls" download="table.xls">

                <div class="face-primary">
                    <span class="icon fa fa-cloud"></span>
                    Download
                </div>

                <div class="face-secondary">
                    <span class="icon fa fa-hdd-o"></span>
                    table.xls
                </div>

            </a>

        </div>




        <!-- _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- -->

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->

</body>

</html>