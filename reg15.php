<?php require_once "includes/functions.php";


function getAllIDs(){
    global $connection;
    $query = "SELECT * FROM students WHERE ID > 214";
    $result = mysqli_query($connection,$query);
    $allStudentsIDs = array();
    while($row = mysqli_fetch_assoc($result)){
        array_push($allStudentsIDs,$row["academic_id"]);
    }
    return $allStudentsIDs;

}


?>

<script>


    function validate15RegForm(){
        var allIDs = <?php echo json_encode(getAllIDs()); ?>;

        var academic_id = document.forms["reg15"]["studentID"].value;

        var fullName = document.forms["reg15"]["fullName"].value;

        var email1 = document.forms["reg15"]["email1"].value;

        var email2 = document.forms["reg15"]["confirm_email"].value;

        if(academic_id == "" || fullName == "" || email1 == "" || email2 == ""){
            alert("Complete the fields");
            return false;
        }

        if(parseInt(academic_id) < 20150000 ){
            alert("You have to be 2015 grade");
            return false;
        }else{

            if(allIDs.indexOf(academic_id) < 0 ){
                alert("This ID is not exist!");
                return false;
            }

            if(email1 != email2){
                alert("Be sure that email fields are identical");
                return false;
            }
        }




    }



</script>



<?php
//$connection = mysqli_connect("localhost","root","","biogrpsys");
$connection = mysqli_connect("localhost","mreyesco_bme","izuqxWZ#gbzb","mreyesco_bme");

function sendMail($email,$password){

    $to = $email;
    $subject = 'BME Department Registration Password';
    $message = '
    Please keep your password in a safe place for future use
    Your Password Is : "'.$password.'"
    For Further Inquiries Contact at "mohamed.abuelanin@gmail.com" or "https://fb.com/mabuelanin"
    ';
    $header = "From: Eng. Mohamed Abualainin <admin@mr-eyes.com>";


    $send = mail($to,$subject,$message,$header);

    if($send == true)
    {
        $msg =  "Mail Sent Successfully To :".$email;
        echo '<script>
        var msg="'.$msg.'";
        console.log(msg);
        </script>';
    }else{
        echo '<script>console.log("Mail Not Sent");</script>';
    }


}


function getUserPassword($academicID){
    global $connection;
    $query = "SELECT student_pass FROM students WHERE academic_id='$academicID'";
    $result = mysqli_query($connection,$query);
    return mysqli_fetch_assoc($result)["student_pass"];
}


function addQuery($academicID,$fullName,$email){
    global $connection;
    $query = "INSERT INTO `reg15` (`ID`, `academic_id`, `full_name`, `email`) VALUES (NULL, '$academicID', '$fullName', '$email')";
    $result = mysqli_query($connection,$query);
}



function checkEmail($email,$academicID){
    global $connection;
    $query = "SELECT * FROM reg15 WHERE email='$email' OR academic_id='$academicID'";
    $result = mysqli_query($connection,$query);
    if(mysqli_num_rows($result) > 0){
        return true;
    }else{
        return false;
    }
}


if(!empty($_POST)){

    $academicID = $_POST["studentID"];
    $fullName = $_POST["fullName"];
    $email = $_POST["email1"];
    $password = getUserPassword($academicID);
    if(checkEmail($email,$academicID)){

        echo '<script language="javascript">';
        echo 'alert("This email or ID is already registered")';
        echo '</script>';

    }else{
        sendMail($email,$password);
        addQuery($academicID,$fullName,$email);
        echo '<script language="javascript">';
        echo 'alert("Successfully Registered")';
        echo '</script>';

    }


}

?>



<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>2015 | Reg</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/loginStyle.css">

</head>
<body>
<div class="row">
    <div class="container">
        <h2 class="title">BME System - HTI</h2>
        <form id="reg15" class="form-signin" method="post" action="reg15.php" name="reg15" onsubmit="return validate15RegForm()">
            <h4 class="section-heading"><span>2015 - Registration</span></h4>
            <!-- Academic ID -->

            <div class="row">
                <div class="column">
                    <label>Academic ID </label>
                    <input id="academicID" class="full-width" type="text" autocomplete="off" maxlength="9"  name="studentID" placeholder="ID" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required autofocus >
                </div>
            </div>

            <!-- Full Name -->

            <div class="row">
                <div class="column">
                    <label>Full name (Arabic) </label>
                    <input id="fullName" class="full-width" type="text" autocomplete="off" name="fullName" placeholder="Full name as on the ID Card" required>
                </div>
            </div>

            <!-- Email -->

            <div class="row">
                <div class="column">
                    <label>Email </label>
                    <input id="email" class="full-width" type="email" autocomplete="off" name="email1" placeholder="Email" required>
                </div>
            </div>
            <!-- Confirm Email -->
            <div class="row">
                <div class="column">
                    <label>Confirm Email </label>
                    <input id="email" class="full-width" type="email" autocomplete="off" name="confirm_email" placeholder="Retype your email" required>
                </div>
            </div>
            <br/>
            <input class="button-submit" type="submit" value="Register">
<!---->
            <div class="row">
<!--                <a href="#" class="forgot">Forgot your password?</a>-->
            </div>
        </form>
    </div>
</div>
</body>
</html>
