<?php // Page Parameters
require_once "includes/functions.php";
$page_name = 'requestOpen.php';
include ("header.php"); ?>

<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/bootstrap-slider.css" rel="stylesheet" type="text/css">

<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap-slider.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>

<script type="text/javascript" src="js/requests_ajax.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/css_auto-reload.js"></script>




<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <br/>
                    Open New Group
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> Opening Request
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<!-- _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- -->

        <form id="subjectCodeForm" onsubmit="return validateSubjectCodeAutocomplete()">
            <div class="ui-widget">
                <label for="automplete-2">Subject Code: </label>
                <input id="automplete-2" name="subjectCode" maxlength="6" size="6">
                <input type="hidden" name="formType" value="select-subjectCode">
                <input type="hidden" name="action" value="open">
                <input type="submit">
            </div>
        </form>

                <div id="changeableForm">
                </div>

<!-- _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- -->

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->

</body>

</html>


<script>
    var subjectCodes = <?php echo json_encode(ListOfSubjects()[0]); ?>;
    $(function() {
        $( "#automplete-2" ).autocomplete({
            minLength:2,
            source: subjectCodes,
            autoFocus:true
        });
    });

</script>